var app 		= require('express')();
var http 		= require('http');
var https		= require('https');
var srv 		= http.Server(app);
var io 			= require('socket.io')(srv);
var popular 	= [];
var groups		= [];
var categories 	= [];
var betlist 	= [];
var allbets 	= [];
// Maybe we don't need to be calling the api so much for demo purposes
var matchcache	= {};
var betcache	= {};

io.on('connection', function(socket){

	socket.on('get popular', function(socket) {
		io.emit('popular list', popular);
	});

	socket.on('get groups', function(socket) {
		io.emit('group list', categories);
	});

	socket.on('get secondarygroup', function(socket) {
		groups.map(function(group, index) {
			if ( group.termKey == socket ) io.emit('secondarygroup list', group);
		});
	});

	socket.on('get matches', function(socket) {

		if ( !matchcache[socket] ) {

			var url = `https://e3-api.kambi.com/offering/api/v3/ub/listView/${socket}.json?lang=fi_FI&market=FI&client_id=2&channel_id=1&ncid=1505905541699&categoryGroup=COMBINED&displayDefault=true`;
			https.get(url, function(res) {

				if ( res.statusCode == 200 ) {
					var body = '';
					res.on('data', (chunk) => {
						body += chunk;
					});
					res.on('end', () => {
						betlist = [];
						var theResponse = JSON.parse(body);
						theResponse.events.map(function(row) {
							betlist.push(row);
						});
						matchcache[socket] = betlist;
						io.emit('bets list', betlist);
					});
				} else {
					console.log('Error');
					return {
						"error": "error"
					}
				}
			});

		} else {
			io.emit('bets list', matchcache[socket]);
			console.log('got mathces from cache');
		}


	});

	// This one actually gets all the advanced bets
	socket.on('get allbets', function(socket) {

		if ( !betcache[socket] ) {

			var url = `https://e3-api.kambi.com/offering/api/v2/ub/betoffer/event/${socket}.json?lang=fi_FI&market=FI&client_id=2&channel_id=1&ncid=1505920610891`;
			https.get(url, function(res) {

				if ( res.statusCode == 200 ) {
					var body = '';
					res.on('data', (chunk) => {
						body += chunk;
					});
					res.on('end', () => {
						allbets = [];
						var theResponse = JSON.parse(body);
						theResponse.betoffers.map(function(row) {
							allbets.push(row);
						});
						betcache[socket] = allbets;
						io.emit('allbets list', allbets);
					});
				} else {
					console.log('Status: ' + res.statusCode);
					return {
						"error": "error"
					}
				}
			}).on('error', (e) => {
				console.log(e);
			});

		} else {
			io.emit('allbets list', betcache[socket]);
			console.log('Got bets from the cache');
		}

	});

	console.log('Connection started');
	socket.on('disconnect', function() {
		console.log('Connection disconnected');
	});
});

srv.listen(4000, function(){
	console.log('listening on *:4000');
});

/*
*****************************************************************************
* Get the landing page content
*****************************************************************************
*/
https.get('https://e1-api.aws.kambicdn.com/offering/api/v2/ub/betoffer/landing.json?lang=fi_FI&market=FI&client_id=2&channel_id=1&ncid=1505481917272', function(res) {

	if ( res.statusCode == 200 ) {
		var body = '';
		res.on('data', (chunk) => {
			body += chunk;
		});
		res.on('end', () => {
			popular = [];
			var theResponse = JSON.parse(body);
			theResponse.result.map(function(row) {
				popular.push(row);
			});
		});
	} else {
		console.log('Error');
		return {
			"error": "error"
		}
	}

}).on('error', function(e) {
	console.log("Error: " + e);
});

/*
*****************************************************************************
* Get all the stuff
*****************************************************************************
*/
https.get('https://e1-api.aws.kambicdn.com/offering/api/v2/ub/group.json?lang=fi_FI&market=FI&channel_id=1&client_id=2&ncid=504843', function(res) {

	if ( res.statusCode == 200 ) {
		var body = '';
		res.on('data', (chunk) => {
			body += chunk;
		});
		res.on('end', () => {
			events = [];
			var theResponse = JSON.parse(body);

			theResponse.group.groups.map(function(group) {
				groups.push(group);
			});

			groups.map(function(group) {
				categories.push({
					name: group.name,
					term: group.termKey,
					sport: group.sport
				});
			});
		});
	} else {
		console.log('Error');
		return {
			"error": "error"
		}		
	}

});


/*
// get the id's from here under popular
https://e1-api.aws.kambicdn.com/offering/api/v2/ub/betoffer/landing.json?lang=fi_FI&market=FI&client_id=2&channel_id=1&ncid=1505481917272

// Call this with the popular game id you want
https://fi.unibet.com/v1/feeds/sportsbook/betoffer/event/1004058318.json?app_id=fc456f86&app_key=9d408d3d085ba9ff6411583f08759315&local=fi_FI&livestreamingcountry=fi&includeparticipants=true&site=fi.unibet.com

https://e1-api.aws.kambicdn.com/offering/api/v2/ub/group.json?lang=fi_FI&market=FI&channel_id=1&client_id=2&ncid=504843

// simple bet offers for list
https://e3-api.kambi.com/offering/api/v3/ub/listView/${socket}.json?lang=fi_FI&market=FI&client_id=2&channel_id=1&ncid=1505905541699&categoryGroup=COMBINED&displayDefault=true

// All the bet offers for event
https://e3-api.kambi.com/offering/api/v2/ub/betoffer/event/1004180681.json?lang=fi_FI&market=FI&client_id=2&channel_id=1&ncid=1505920610891
*/